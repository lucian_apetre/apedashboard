# README #

ape3dashboard

install requirements:
```
pip install flask flask-sqlalchemy flask-login psycopg2-binary
```
restart app:
```
python
from project import db, create_app
db.create_all(app=create_app())
```

run:
```
cd ~/ape3codelab/apedashboard/apedashboard
export FLASK_APP=project
flask run
```