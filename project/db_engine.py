from project import constants
from project.models import LiveJasminLog, BimBimLog, LiveJasminModel, BimBimModel, StripchatLog, XLoveLog
from project import constants


class LiveJasminHelper:

    @staticmethod
    def get_payment_per_last_days(model_name, days_back=0):
        if days_back == 0:
            d_month_ago = constants.format_today()
        else:
            d_month_ago = constants.format_time_ago(days_back)

        # detect all values for today
        result = (LiveJasminLog.query.
                  filter(LiveJasminLog.timestamp >= d_month_ago).
                  order_by(LiveJasminLog.timestamp.asc()).
                  filter_by(username=model_name)).all()
        if result is None:
            return None
        date_item = None
        data = None
        for item in result:
            # print(item)
            start_day, tmp_date_time1, tmp_date_time2, tmp_date_time3, end_day = constants.timestamp_to_intervals(
                item.timestamp)

            # if first element
            if date_item is None:
                date_item = item.timestamp.date()
                data = {date_item: {}}
            # if last saved one != new date
            elif date_item != item.timestamp.date():
                date_item = item.timestamp.date()
                data[date_item] = {}

            if start_day <= item.timestamp < tmp_date_time1:
                if 'i1' in data[date_item]:
                    data[date_item].update({'i1': item})
                else:
                    data[date_item]['i1'] = item
            elif tmp_date_time1 <= item.timestamp < tmp_date_time2:
                if 'i2' in data[date_item]:
                    data[date_item].update({'i2': item})
                else:
                    data[date_item]['i2'] = item
            elif tmp_date_time2 <= item.timestamp <= tmp_date_time3:
                if 'i3' in data[date_item]:
                    data[date_item].update({'i3': item})
                else:
                    data[date_item]['i3'] = item
            elif tmp_date_time3 <= item.timestamp <= end_day:
                if 'i4' in data[date_item]:
                    data[date_item].update({'i4': item})
                else:
                    data[date_item]['i4'] = item
            else:
                print(f'something went wrong with {item.timestamp}')
            continue
        return data

    @staticmethod
    def get_payment(model_name, start_date, end_date):
        model_data = LiveJasminHelper.get_payment_per_date_interval(model_name, start_date, end_date)
        if model_data is None:
            return None
        model_data = constants.append_total_data(model_data)
        template_table = constants.create_income_table_template(start_date=start_date, end_date=end_date)
        complete_table = constants.append_payment_data(template_table, model_data)
        return complete_table

    @staticmethod
    def get_payment_per_date_interval(model_name, start_date, end_date):
        result = (LiveJasminLog.query.
                  filter(LiveJasminLog.timestamp >= start_date).
                  filter(LiveJasminLog.timestamp <= end_date).
                  order_by(LiveJasminLog.timestamp.asc()).
                  filter_by(username=model_name)).all()
        if result is None:
            return None
        date_item = None
        data = None
        for item in result:
            # print(item)
            start_day, tmp_date_time1, tmp_date_time2, tmp_date_time3, end_day = constants.timestamp_to_intervals(
                item.timestamp)

            # if first element
            if date_item is None:
                date_item = item.timestamp.date()
                data = {date_item: {}}
            # if last saved one != new date
            elif date_item != item.timestamp.date():
                date_item = item.timestamp.date()
                data[date_item] = {}

            if start_day <= item.timestamp < tmp_date_time1:
                if 'i1' in data[date_item]:
                    data[date_item].update({'i1': item})
                else:
                    data[date_item]['i1'] = item
            elif tmp_date_time1 <= item.timestamp < tmp_date_time2:
                if 'i2' in data[date_item]:
                    data[date_item].update({'i2': item})
                else:
                    data[date_item]['i2'] = item
            elif tmp_date_time2 <= item.timestamp <= tmp_date_time3:
                if 'i3' in data[date_item]:
                    data[date_item].update({'i3': item})
                else:
                    data[date_item]['i3'] = item
            elif tmp_date_time3 <= item.timestamp <= end_day:
                if 'i4' in data[date_item]:
                    data[date_item].update({'i4': item})
                else:
                    data[date_item]['i4'] = item
            else:
                print(f'something went wrong with {item.timestamp}')
            continue
        return data

    @staticmethod
    def get_payment_data_period(model_name, start_date, end_date):
        # get all data from the period
        result = (LiveJasminLog.query.
                  filter(LiveJasminLog.timestamp >= start_date).
                  filter(LiveJasminLog.timestamp <= end_date).
                  order_by(LiveJasminLog.timestamp.asc()).
                  filter_by(username=model_name)).all()
        if result is None:
            return None
        date_item = None
        data = None
        # go through all results and update amounts per day so we have the latest recorded amount per day
        for item in result:
            # if first element
            if date_item is None:
                date_item = item.timestamp.date()
                data = {date_item: {}}
            # if last saved one != new date
            elif date_item != item.timestamp.date():
                date_item = item.timestamp.date()
                data[date_item] = {}
            data[date_item] = item

        # compute and add total per model
        total_all = 0
        for day in data:
            total_all += data[day].amount
        data['t_all'] = round(total_all, 2)
        return data

    @staticmethod
    def get_payment_period(model_name, start_date, end_date):
        model_data = LiveJasminHelper.get_payment_data_period(model_name, start_date, end_date)
        template_table = constants.create_income_table_template(start_date=start_date, end_date=end_date)
        complete_table = constants.append_payment_data(template_table, model_data)
        return complete_table

    @staticmethod
    def _get_payment_data_period_total(model_name, start_date, end_date):
        # get all data from the period
        result = (LiveJasminLog.query.
                  filter(LiveJasminLog.timestamp >= start_date).
                  filter(LiveJasminLog.timestamp <= end_date).
                  order_by(LiveJasminLog.timestamp.asc()).
                  filter_by(username=model_name)).all()
        if result is None or len(result) == 0:
            return 0
        date_item = None
        data = None
        # go through all results and update amounts per day so we have the latest recorded amount per day
        for item in result:
            # if first element
            if date_item is None:
                date_item = item.timestamp.date()
                data = {date_item: {}}
            # if last saved one != new date
            elif date_item != item.timestamp.date():
                date_item = item.timestamp.date()
                data[date_item] = {}
            data[date_item] = item

        # compute and add total per model
        total_all = 0
        for day in data:
            total_all += data[day].amount
        # data['t_all'] = round(total_all, 2)
        return round(total_all, 2)

    # gel all models with corresponding heylux account
    @staticmethod
    def _get_unique_models():
        result = (LiveJasminModel.query.
                  order_by(LiveJasminModel.studio_username)).all()

        return result if result else []

    @staticmethod
    def get_payment_period_all(start_date, end_date):
        data = {}
        for model in LiveJasminHelper._get_unique_models():
            amount = LiveJasminHelper._get_payment_data_period_total(model.livejasmin_username, start_date, end_date)
            data[model.studio_username] = amount
        return data


class BaseHelper:

    # one site at a time, compute total amount of USD/EUR/.. from all models
    # returns a dict with keys: model_name and values: amount(USD or EUR)
    @staticmethod
    def get_payment_period_all(start_date, end_date, target_model_table, taget_log_table):
        data = {}
        for model in BaseHelper._get_unique_models(target_model_table):
            amount = 0
            if taget_log_table == BimBimLog:
                amount = BaseHelper._get_payment_data_period_total(model.bimbim_username, start_date, end_date, taget_log_table)
            elif taget_log_table == LiveJasminLog:
                amount = BaseHelper._get_payment_data_period_total(model.livejasmin_username, start_date, end_date, taget_log_table)
            elif taget_log_table == StripchatLog:
                amount = BaseHelper._get_payment_data_period_total(model.stripchat_username, start_date, end_date,
                                                                   taget_log_table)
            elif taget_log_table == XLoveLog:
                amount = BaseHelper._get_payment_data_period_total(model.xlove_username, start_date, end_date, taget_log_table)
            data[model.studio_username] = amount
        return data

    # gel all models with corresponding heylux account
    @staticmethod
    def _get_unique_models(target_model_table):
        result = (target_model_table.query.
                  order_by(target_model_table.studio_username)).all()

        return result if result else []

    @staticmethod
    def _get_payment_data_period_total(model_name, start_date, end_date, taget_log_table):
        # get all data from the period
        result = (taget_log_table.query.
                  filter(taget_log_table.timestamp >= start_date).
                  filter(taget_log_table.timestamp <= end_date).
                  order_by(taget_log_table.timestamp.asc()).
                  filter_by(username=model_name)).all()
        if result is None or len(result) == 0:
            return 0
        date_item = None
        data = None
        # go through all results and update amounts per day so we have the latest recorded amount per day
        for item in result:
            # if first element
            if date_item is None:
                date_item = item.timestamp.date()
                data = {date_item: {}}
            # if last saved one != new date
            elif date_item != item.timestamp.date():
                date_item = item.timestamp.date()
                data[date_item] = {}
            data[date_item] = item

        # compute and add total per model
        total_all = 0
        for day in data:
            total_all += data[day].amount
        # data['t_all'] = round(total_all, 2)
        return round(total_all, 2)

    @staticmethod
    def get_payment(model_name, start_date, end_date, target_log_db):
        model_data = BaseHelper.get_payment_per_date_interval(model_name, start_date, end_date, target_log_db)
        if model_data is None:
            return None
        model_data = constants.append_total_data(model_data)
        template_table = constants.create_income_table_template(start_date=start_date, end_date=end_date)
        complete_table = constants.append_payment_data(template_table, model_data)
        return complete_table

    @staticmethod
    def get_payment_per_date_interval(model_name, start_date, end_date, log_table):
        result = (log_table.query.
                  filter(log_table.timestamp >= start_date).
                  filter(log_table.timestamp <= end_date).
                  order_by(log_table.timestamp.asc()).
                  filter_by(username=model_name)).all()
        if result is None:
            return None
        data = BaseHelper.split_intervals(result)
        return data

    @staticmethod
    def split_intervals(payment_data):
        date_item = None
        data = None
        for item in payment_data:
            # print(item)
            start_day, tmp_date_time1, tmp_date_time2, tmp_date_time3, end_day = constants.timestamp_to_intervals(
                item.timestamp)

            # if first element
            if date_item is None:
                date_item = item.timestamp.date()
                data = {date_item: {}}
            # if last saved one != new date
            elif date_item != item.timestamp.date():
                date_item = item.timestamp.date()
                data[date_item] = {}

            if start_day <= item.timestamp < tmp_date_time1:
                if 'i1' in data[date_item]:
                    data[date_item].update({'i1': item})
                else:
                    data[date_item]['i1'] = item
            elif tmp_date_time1 <= item.timestamp < tmp_date_time2:
                if 'i2' in data[date_item]:
                    data[date_item].update({'i2': item})
                else:
                    data[date_item]['i2'] = item
            elif tmp_date_time2 <= item.timestamp <= tmp_date_time3:
                if 'i3' in data[date_item]:
                    data[date_item].update({'i3': item})
                else:
                    data[date_item]['i3'] = item
            elif tmp_date_time3 <= item.timestamp <= end_day:
                if 'i4' in data[date_item]:
                    data[date_item].update({'i4': item})
                else:
                    data[date_item]['i4'] = item
            else:
                print(f'something went wrong with {item.timestamp}')
            continue
        return data


class BimBimHelper:

    @staticmethod
    def get_payment(model_name, start_date, end_date):
        model_data = BimBimHelper.get_payment_per_date_interval(model_name, start_date, end_date)
        if model_data is None:
            return None
        model_data = constants.append_total_data(model_data)
        template_table = constants.create_income_table_template(start_date=start_date, end_date=end_date)
        complete_table = constants.append_payment_data(template_table, model_data)
        return complete_table

    @staticmethod
    def get_payment_per_date_interval(model_name, start_date, end_date):
        result = (BimBimLog.query.
                  filter(BimBimLog.timestamp >= start_date).
                  filter(BimBimLog.timestamp <= end_date).
                  order_by(BimBimLog.timestamp.asc()).
                  filter_by(username=model_name)).all()
        if result is None:
            return None
        data = BaseHelper.split_intervals(result)
        return data

    @staticmethod
    def _get_payment_data_period_total(model_name, start_date, end_date):
        # get all data from the period
        result = (BimBimLog.query.
                  filter(BimBimLog.timestamp >= start_date).
                  filter(BimBimLog.timestamp <= end_date).
                  order_by(BimBimLog.timestamp.asc()).
                  filter_by(username=model_name)).all()
        if result is None or len(result) == 0:
            return 0
        date_item = None
        data = None
        # go through all results and update amounts per day so we have the latest recorded amount per day
        for item in result:
            # if first element
            if date_item is None:
                date_item = item.timestamp.date()
                data = {date_item: {}}
            # if last saved one != new date
            elif date_item != item.timestamp.date():
                date_item = item.timestamp.date()
                data[date_item] = {}
            data[date_item] = item

        # compute and add total per model
        total_all = 0
        for day in data:
            total_all += data[day].amount
        # data['t_all'] = round(total_all, 2)
        return round(total_all, 2)

    # gel all models with corresponding heylux account
    @staticmethod
    def _get_unique_models():
        result = (BimBimModel.query.
                  order_by(BimBimModel.studio_username)).all()

        return result if result else []

    @staticmethod
    def get_payment_period_all(start_date, end_date):
        data = {}
        for model in BimBimHelper._get_unique_models():
            amount = BimBimHelper._get_payment_data_period_total(model.bimbim_username, start_date, end_date)
            data[model.studio_username] = amount
        return data
