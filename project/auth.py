from flask import Blueprint, render_template, redirect, url_for, request, flash
from werkzeug.security import generate_password_hash, check_password_hash
from .models import User, ModelInfo, LiveJasminLog, BimBimLog, LiveJasminModel, BimBimModel, StripchatModel, \
    StripchatLog, ChaturbateLog, ChaturbatetModel, XLoveLog, XLoveModel
from project import constants
from project.db_engine import LiveJasminHelper, BimBimHelper, BaseHelper
from datetime import datetime, date, timedelta

from flask_login import login_user, logout_user, login_required
from . import db

auth = Blueprint('auth', __name__)


@auth.route('/login')
def login():
    return render_template('login.html')


@auth.route('/login', methods=['POST'])
def login_post():
    email = request.form.get('email')
    password = request.form.get('password')
    remember = True if request.form.get('remember') else False

    user = User.query.filter_by(email=email).first()

    # check if the user actually exists
    # take the user-supplied password, hash it, and compare it to the hashed password in the database
    if not user or not check_password_hash(user.password, password):
        flash('Please check your login details and try again.')
        return redirect(url_for('auth.login'))  # if the user doesn't exist or password is wrong, reload the page

    # if the above check passes, then we know the user has the right credentials
    login_user(user, remember=remember)
    return redirect(url_for('main.profile'))


@auth.route('/signup')
def signup():
    return render_template('signup.html')


@auth.route('/signup', methods=['POST'])
def signup_post():
    email = request.form.get('email')
    name = request.form.get('name')
    password = request.form.get('password')

    user = User.query.filter_by(
        email=email).first()  # if this returns a user, then the email already exists in database

    if user:  # if a user is found, we want to redirect back to signup page so user can try again
        flash('Email address already exists')
        return redirect(url_for('auth.signup'))

    # create a new user with the form data. Hash the password so the plaintext version isn't saved.
    new_user = User(email=email, name=name, password=generate_password_hash(password, method='sha256'))

    # add the new user to the database
    db.session.add(new_user)
    db.session.commit()

    return redirect(url_for('auth.login'))


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('main.index'))


@auth.route('/add-model', methods=['GET', 'POST'])
@login_required
def add_model():
    if request.method == 'GET':
        return render_template('add-model.html')
    if request.method == 'POST':
        username = request.form.get('username')
        email = request.form.get('email')
        first_name = request.form.get('first_name')
        last_name = request.form.get('last_name')

        # check if already exists
        old_model_email = ModelInfo.query.filter_by(email=email).first()

        if old_model_email:  # if a user is found, we want to redirect back to signup page so user can try again
            flash('model with same email address already exists')
            return render_template('add-model.html')

        # check if already exists
        old_model_username = ModelInfo.query.filter_by(username=username).first()

        if old_model_username:  # if a user is found, we want to redirect back to signup page so user can try again
            flash('model with same username already exists')
            return render_template('add-model.html')

        new_model = ModelInfo(_username=username,
                              _email=email,
                              _first_name=first_name,
                              _last_name=last_name)

        # add the new user to the database
        db.session.add(new_model)
        db.session.commit()

        flash('OK')

        return render_template('add-model.html')


@auth.route('/view-all-models')
@login_required
def view_all_models():
    # get all models from DB
    models_data_raw = (ModelInfo.query.
                       with_entities(ModelInfo.username, ModelInfo.first_name, ModelInfo.last_name, ModelInfo.is_active,
                                     LiveJasminModel.livejasmin_username, BimBimModel.bimbim_username,
                                     StripchatModel.stripchat_username, ChaturbatetModel.chaturbate_username,
                                     XLoveModel.xlove_username).
                       join(LiveJasminModel, LiveJasminModel.studio_username == ModelInfo.username, isouter=True).
                       join(BimBimModel, BimBimModel.studio_username == ModelInfo.username, isouter=True).
                       join(StripchatModel, StripchatModel.studio_username == ModelInfo.username, isouter=True).
                       join(ChaturbatetModel, ChaturbatetModel.studio_username == ModelInfo.username, isouter=True).
                       join(XLoveModel, XLoveModel.studio_username == ModelInfo.username, isouter=True)).all()

    # models_data = ModelInfo.query.all()
    return render_template('view-all-models.html', models_data=models_data_raw)


@auth.route('/link-models', methods=['GET', 'POST'])
@login_required
def link_models():
    data = {}
    # heylux models accounts
    heylux_models_bulk = (ModelInfo.query.
                          with_entities(ModelInfo.username).
                          order_by(ModelInfo.username)).all()
    heylux_models = [model_name[0] for model_name in heylux_models_bulk] if heylux_models_bulk else []
    data['heylux'] = heylux_models

    # LiveJasmin: extract all unique mode names from the last month
    d_month_ago = constants.format_time_ago(30)
    live_jasmin_models_bulk = (LiveJasminLog.query.
                               with_entities(LiveJasminLog.username).
                               filter(LiveJasminLog.timestamp >= d_month_ago).
                               distinct().
                               order_by(LiveJasminLog.username)).all()

    live_jasmin_models = [model_name[0] for model_name in live_jasmin_models_bulk] if live_jasmin_models_bulk else []
    data['live_jasmin'] = live_jasmin_models

    # BimBim: extract all unique mode names from the last month
    d_month_ago = constants.format_time_ago(30)
    bimbim_models_bulk = (BimBimLog.query.
                          with_entities(BimBimLog.username).
                          filter(BimBimLog.timestamp >= d_month_ago).
                          distinct().
                          order_by(BimBimLog.username)).all()

    bimbim_models = [model_name[0] for model_name in bimbim_models_bulk] if bimbim_models_bulk else []
    data['bimbim'] = bimbim_models

    # Stripchat: extract all unique mode names from the last month
    # d_month_ago = constants.format_time_ago(30)
    d_month_ago = date.today() - timedelta(days=30)
    stripchat_models_bulk = (StripchatLog.query.
                             with_entities(StripchatLog.username).
                             filter(StripchatLog.timestamp >= d_month_ago).
                             distinct().
                             order_by(StripchatLog.username)).all()

    stripchat_models = [model_name[0] for model_name in stripchat_models_bulk] if stripchat_models_bulk else []
    data['stripchat'] = stripchat_models

    # XLove: extract all unique mode names from the last month
    d_month_ago = date.today() - timedelta(days=30)
    xlove_models_bulk = (XLoveLog.query.
                         with_entities(XLoveLog.username).
                         filter(XLoveLog.timestamp >= d_month_ago).
                         distinct().
                         order_by(XLoveLog.username)).all()

    xlove_models = [model_name[0] for model_name in xlove_models_bulk] if xlove_models_bulk else []
    data['xlove'] = xlove_models

    if request.method == 'POST':
        success = True
        username_heylux = request.form.get('heylux_sel')
        username_jasmin = request.form.get('jasmin_sel')
        username_bimbim = request.form.get('bimbim_sel')
        username_stripchat = request.form.get('stripchat_sel')
        username_chaturbate = request.form.get('chaturbate_username')
        chaturbate_apikey = request.form.get('chaturbate_apikey')
        username_xlove = request.form.get('xlove_sel')

        if username_heylux == 'Select heylux user':
            flash(f'you must select a heylux user and at least one other user to link')
            return render_template('link-models.html', models_data=data)
        if username_jasmin == 'Select LiveJasmin user':
            username_jasmin = None
        if username_bimbim == 'Select BimBim user':
            username_bimbim = None
        if username_stripchat == 'Select Stripchat user':
            username_stripchat = None
        if username_chaturbate == 'Insert Chaturbate user':
            username_chaturbate = None
        if username_jasmin is None and username_bimbim is None and username_stripchat is None and username_chaturbate is None:
            flash(f'you must select at least one user to link with heylux user')
        if username_xlove == 'Insert XLove user':
            username_xlove = None

        # we have a heylux user and at least one web-studio user
        if username_jasmin:
            # check if already exists
            old_model_username = LiveJasminModel.query.filter_by(livejasmin_username=username_jasmin).first()

            if old_model_username:  # if a user is found, we want to redirect back to signup page so user can try again
                flash(f'LiveJasmin: model with username {username_jasmin} already exists')
                success = False
            else:
                new_model = LiveJasminModel(_livejasminj_user=username_jasmin,
                                            _local_user=username_heylux)

                # add the new user to the database
                db.session.add(new_model)
                db.session.commit()

        if username_bimbim:
            # check if already exists
            old_model_username = BimBimModel.query.filter_by(bimbim_username=username_bimbim).first()

            if old_model_username:  # if a user is found, we want to redirect back to signup page so user can try again
                flash(f'BimBim: model with username {username_bimbim} already exists')
                success = False
            else:
                new_model = BimBimModel(_bimbim_user=username_bimbim,
                                        _local_user=username_heylux)

                # add the new user to the database
                db.session.add(new_model)
                db.session.commit()

        if username_stripchat:
            # check if already exists
            old_model_username = StripchatModel.query.filter_by(stripchat_username=username_stripchat).first()

            if old_model_username:  # if a user is found, we want to redirect back so user can try again
                flash(f'Stripchat: model with username {username_stripchat} already exists')
                success = False
            else:
                new_model = StripchatModel(_stripchat_user=username_stripchat,
                                           _local_user=username_heylux)

                # add the new user to the database
                db.session.add(new_model)
                db.session.commit()

        if username_chaturbate:
            # check if already exists
            old_model_username = ChaturbatetModel.query.filter_by(chaturbate_username=username_chaturbate).first()

            if old_model_username:  # if a user is found, we want to redirect back so user can try again
                flash(f'Chaturbate: model with username {username_chaturbate} already exists')
                success = False
            else:
                if constants.check_chaturbate_apikey(username_chaturbate, chaturbate_apikey):
                    new_model = ChaturbatetModel(_chaturbate_user=username_chaturbate,
                                                 _local_user=username_heylux,
                                                 _apikey=chaturbate_apikey)

                    # add the new user to the database
                    db.session.add(new_model)
                    db.session.commit()
                else:
                    flash(
                        f'Chaturbate: failed to check username {username_chaturbate} with apikey: {chaturbate_apikey}')
        if username_xlove:
            # check if already exists
            old_model_username = XLoveModel.query.filter_by(xlove_username=username_xlove).first()

            if old_model_username:  # if a user is found, we want to redirect back so user can try again
                flash(f'XLove: model with username {username_xlove} already exists')
                success = False
            else:
                new_model = XLoveModel(_xlove_user=username_xlove,
                                       _local_user=username_heylux)

                # add the new user to the database
                db.session.add(new_model)
                db.session.commit()

        if success:
            flash('perfect')
    return render_template('link-models.html', models_data=data)


@auth.route('/balance', methods=['GET', 'POST'])
@login_required
def balance():
    # heylux models accounts
    heylux_models_bulk = (ModelInfo.query.
                          with_entities(ModelInfo.username).
                          order_by(ModelInfo.username)).all()
    heylux_models = [model_name[0] for model_name in heylux_models_bulk] if heylux_models_bulk else []
    date_periods = constants.get_last_periods()
    date_periods_str = constants.date_intervals_to_str(date_periods)

    if request.method == 'GET':
        return render_template('balance_get.html', heylux_models=heylux_models, periods_str=date_periods_str)
    if request.method == 'POST':
        username_heylux = request.form.get('heylux_sel')
        start_date = request.form.get('from_date')
        to_date = request.form.get('to_date')

        if username_heylux == 'Select user':
            flash(f'you must select a heylux user')
            return render_template('balance_get.html', heylux_models=heylux_models)
        if start_date is None or to_date is None:
            flash(f'you must select a start date and an end date')
            return render_template('balance_get.html', heylux_models=heylux_models)
        d_start_date = datetime.strptime(start_date, '%Y-%m-%d')
        d_end_date = datetime.strptime(to_date, '%Y-%m-%d')
        d_end_date = datetime(d_end_date.year, d_end_date.month, d_end_date.day, 23, 59, 59,
                              999999)  # add one more day so it will include also current day until 23:59

        if d_start_date > d_end_date:
            flash(f'FROM date should be before TO date')
            return render_template('balance_get.html', heylux_models=heylux_models)

        flash(f'Selected user {username_heylux}')

        # get LiveJasmin model associated with heylux user
        live_jasmin_model = LiveJasminModel.query.with_entities(LiveJasminModel.livejasmin_username).filter_by(
            studio_username=username_heylux).first()
        lj_payment_data = LiveJasminHelper.get_payment(live_jasmin_model.livejasmin_username, d_start_date,
                                                       d_end_date) if live_jasmin_model else []

        # get BimBim model associated with heylux user
        bimbim_model = BimBimModel.query.with_entities(BimBimModel.bimbim_username).filter_by(
            studio_username=username_heylux).first()
        bb_payment_data = BimBimHelper.get_payment(bimbim_model.bimbim_username, d_start_date,
                                                   d_end_date) if bimbim_model else []

        # get Stripchat model associated with heylux user
        stripchat_model = StripchatModel.query.with_entities(StripchatModel.stripchat_username).filter_by(
            studio_username=username_heylux).first()
        strip_payment_data = BaseHelper.get_payment(stripchat_model.stripchat_username,
                                                    d_start_date,
                                                    d_end_date,
                                                    StripchatLog) if stripchat_model else []

        # get Chaturbate model associated with heylux user
        chaturbate_model = ChaturbatetModel.query.with_entities(ChaturbatetModel.chaturbate_username).filter_by(
            studio_username=username_heylux).first()
        chaturbate_payment_data = BaseHelper.get_payment(chaturbate_model.chaturbate_username,
                                                         d_start_date,
                                                         d_end_date,
                                                         ChaturbateLog) if chaturbate_model else []

        # get XLove model associated with heylux user
        xlove_model = XLoveModel.query.with_entities(XLoveModel.xlove_username).filter_by(
            studio_username=username_heylux).first()
        xlove_payment_data = BaseHelper.get_payment(xlove_model.xlove_username,
                                                    d_start_date,
                                                    d_end_date,
                                                    XLoveLog) if xlove_model else []

        # get a template table with all intervals so we can iterate with it
        template_table = constants.create_income_table_template(start_date=d_start_date, end_date=d_end_date)
        return render_template('balance_details.html',
                               template_data=template_table,
                               lj_data=lj_payment_data,
                               bb_data=bb_payment_data,
                               strip_data=strip_payment_data,
                               chaturbate_data=chaturbate_payment_data,
                               xlove_data=xlove_payment_data)


@auth.route('/balance-period', methods=['POST'])
@login_required
def balance_period():
    date_periods = constants.get_last_periods()
    date_periods_str = constants.date_intervals_to_str(date_periods)

    period_sel = request.form.get('period_sel')
    if period_sel == 'Select period':
        # heylux models accounts
        heylux_models_bulk = (ModelInfo.query.
                              with_entities(ModelInfo.username).
                              order_by(ModelInfo.username)).all()
        heylux_models = [model_name[0] for model_name in heylux_models_bulk] if heylux_models_bulk else []

        flash('you must select a period')
        return render_template('balance_get.html', heylux_models=heylux_models, periods_str=date_periods_str)

    selected_period_index = date_periods_str.index(period_sel)

    # selected_period_start, selected_period_end = date_periods[selected_period_index]
    periods = date_periods[selected_period_index]
    if len(periods) == 2:
        d_end_date = datetime(periods[1].year, periods[1].month, periods[1].day, 23, 59, 59,
                              999999)  # add one more day so it will include also current day until 23:59
    else:
        today = date.today()
        d_end_date = datetime(today.year, today.month, today.day, 23, 59, 59, 999999)  # add one more day so it will
        # include also current day until 23:59
    lj_all = LiveJasminHelper.get_payment_period_all(periods[0], d_end_date)
    bb_all = BimBimHelper.get_payment_period_all(periods[0], d_end_date)
    strip_all = BaseHelper.get_payment_period_all(periods[0], d_end_date, target_model_table=StripchatModel,
                                                  taget_log_table=StripchatLog)
    chat_all = BaseHelper.get_payment_period_all(periods[0], d_end_date, target_model_table=ChaturbatetModel,
                                                 taget_log_table=ChaturbateLog)
    xlove_all_eur = BaseHelper.get_payment_period_all(periods[0], d_end_date, target_model_table=XLoveModel,
                                                      taget_log_table=XLoveLog)
    # contains general information like total per SITE: LJ, total for BB, information is displayed as a new line in
    # html template; currency USD
    row_total_usd = constants.build_totals_vertical_usd(lj_all, bb_all, strip_all, chat_all)
    # new total for EUR
    row_total_eur = constants.build_totals_vertical_eur(xlove_all_eur)

    # create a union for all models so we can have a 'template'
    union_models = list(lj_all.keys() | bb_all.keys() | strip_all.keys() | chat_all.keys() | xlove_all_eur.keys())

    # contains general information per model including total per studio; USD sites
    column_total_usd = constants.build_totals_horizontal_usd(union_models, lj_all, bb_all, strip_all, chat_all)

    # contains general information per model including total per studio; EUR sites
    column_total_eur = constants.build_totals_horizontal_eur(union_models, xlove_all_eur)

    usd_ron = constants.get_currency_usd_ron(d_end_date)
    eur_usd = constants.get_currency_eur_usd(d_end_date)
    return render_template('balance_period.html',
                           selected_period=period_sel,
                           lj_data=lj_all,
                           bb_data=bb_all,
                           strip_data=strip_all,
                           chat_data=chat_all,
                           xlove_data=xlove_all_eur,
                           union_data=union_models,
                           general_data_vertical=row_total_usd,
                           general_data_horizontal=column_total_usd,
                           general_data_vertical_eur=row_total_eur,
                           general_data_horizontal_eur=column_total_eur,
                           usd_ron=usd_ron,
                           eur_usd=eur_usd)
