from flask_login import UserMixin
from . import db
from datetime import datetime

ACCESS = {
    'model': 0,
    'mentor': 1,
    'admin': 2
}


class User(UserMixin, db.Model):
    __tablename__ = 'heylux_users'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))
    name = db.Column(db.String(1000))
    access = db.Column('access', db.SmallInteger, db.CheckConstraint("access IN (0, 1, 2)"), default=0)

    def is_admin(self):
        return self.access == ACCESS['admin']


class ModelInfo(db.Model):
    __tablename__ = 'models_info'

    id = db.Column('id', db.Integer, db.Sequence('model_info_pk_seq'), primary_key=True)
    # actual data
    username = db.Column('username', db.String, nullable=False, unique=True)
    email = db.Column('email', db.String, nullable=True, unique=True)
    first_name = db.Column('first_name', db.String, nullable=True)
    last_name = db.Column('last_name', db.String, nullable=True)
    start_date = db.Column('start_date', db.DateTime, nullable=True)
    end_date = db.Column('end_date', db.DateTime, nullable=True)
    is_active = db.Column('is_active', db.Boolean, nullable=True)

    def __init__(self, _username, _email, _first_name, _last_name):
        self.username = _username
        self.email = _email
        self.first_name = _first_name
        self.last_name = _last_name
        self.start_date = datetime.now()
        self.is_active = True

    def __repr__(self):
        return f' username:{self.username}\n email: {self.email}\n first_name: {self.first_name}\n last_name{self.last_name}'


class LiveJasminLog(db.Model):
    __tablename__ = 'livejasmin_logs'

    id = db.Column('id', db.Integer, db.Sequence('livejasmin_logs_pk_seq'), primary_key=True)
    timestamp = db.Column('timestamp', db.DateTime, nullable=False)
    username = db.Column('username', db.String, nullable=False)
    amount = db.Column('amount', db.Float(precision=2), nullable=False)

    def __repr__(self):
        return f'id: {self.id}, timestamp: {self.timestamp}, username: {self.username}, amount: {self.amount}'


# keeps a mapping of LiveJasmin models to studio models
# this will be useful to create once we have all models by name so we can easily query the DB by LiveJasmin site
class LiveJasminModel(db.Model):
    __tablename__ = 'livejasmin_models'
    id = db.Column('id', db.Integer, db.Sequence('livejasmin_models_pk_seq'), primary_key=True)

    livejasmin_username = db.Column('livejasmin_username', db.String)
    studio_username = db.Column('studio_username', db.String, db.ForeignKey(ModelInfo.username))

    def __init__(self, _livejasminj_user, _local_user):
        self.livejasmin_username = _livejasminj_user
        self.studio_username = _local_user

    def __repr__(self):
        return f'id: {self.id}, livejasmin_username: {self.livejasmin_username}, studio_username: {self.studio_username}'


class BimBimLog(db.Model):
    __tablename__ = 'bimbim_logs'

    id = db.Column('id', db.Integer, db.Sequence('bimbim_logs_pk_seq'), primary_key=True)
    timestamp = db.Column('timestamp', db.DateTime, nullable=False)
    username = db.Column('username', db.String, nullable=False)
    amount = db.Column('amount', db.Float(precision=2), nullable=False)

    def __repr__(self):
        return f'id: {self.id}, timestamp: {self.timestamp}, username: {self.username}, amount: {self.amount}'


# keeps a mapping of bimbim models to studio models
# this will be useful to create once we have all models by name so we can easily query the DB by BimBim site
class BimBimModel(db.Model):
    __tablename__ = 'bimbim_models'
    id = db.Column('id', db.Integer, db.Sequence('bimbim_models_pk_seq'), primary_key=True)

    bimbim_username = db.Column('bimbim_username', db.String)
    studio_username = db.Column('studio_username', db.String, db.ForeignKey(ModelInfo.username))

    def __init__(self, _bimbim_user, _local_user):
        self.bimbim_username = _bimbim_user
        self.studio_username = _local_user

    def __repr__(self):
        return f'id: {self.id}, bimbim_username: {self.bimbim_username}, studio_username: {self.studio_username}'


class StripchatLog(db.Model):
    __tablename__ = 'stripchat_logs'

    id = db.Column('id', db.Integer, db.Sequence('stripchat_logs_pk_seq'), primary_key=True)
    timestamp = db.Column('timestamp', db.DateTime, nullable=False)
    username = db.Column('username', db.String, nullable=False)
    amount = db.Column('amount', db.Float(precision=2), nullable=False)

    def __repr__(self):
        return f'id: {self.id}, timestamp: {self.timestamp}, username: {self.username}, amount: {self.amount}'


# keeps a mapping of stripchat models to studio models
# this will be useful to create once we have all models by name so we can easily query the DB by stripchat site
class StripchatModel(db.Model):
    __tablename__ = 'stripchat_models'
    id = db.Column('id', db.Integer, db.Sequence('stripchat_models_pk_seq'), primary_key=True)

    stripchat_username = db.Column('stripchat_username', db.String)
    studio_username = db.Column('studio_username', db.String, db.ForeignKey(ModelInfo.username))

    def __init__(self, _stripchat_user, _local_user):
        self.stripchat_username = _stripchat_user
        self.studio_username = _local_user

    def __repr__(self):
        return f'id: {self.id}, stripchat_username: {self.stripchat_username}, studio_username: {self.studio_username}'


class ChaturbateLog(db.Model):
    __tablename__ = 'chaturbate_logs'

    id = db.Column('id', db.Integer, db.Sequence('chaturbate_logs_pk_seq'), primary_key=True)
    timestamp = db.Column('timestamp', db.DateTime, nullable=False)
    username = db.Column('username', db.String, nullable=False)
    amount = db.Column('amount', db.Float(precision=2), nullable=False)

    def __repr__(self):
        return f'id: {self.id}, timestamp: {self.timestamp}, username: {self.username}, amount: {self.amount}'


# keeps a mapping of chaturbate models to studio models
# this will be useful to create once we have all models by name so we can easily query the DB by chaturbate site
class ChaturbatetModel(db.Model):
    __tablename__ = 'chaturbate_models'
    id = db.Column('id', db.Integer, db.Sequence('chaturbate_models_pk_seq'), primary_key=True)

    chaturbate_username = db.Column('chaturbate_username', db.String)
    studio_username = db.Column('studio_username', db.String, db.ForeignKey(ModelInfo.username))
    apikey = db.Column('apikey', db.String)

    def __init__(self, _chaturbate_user, _local_user, _apikey):
        self.chaturbate_username = _chaturbate_user
        self.studio_username = _local_user
        self.apikey = _apikey

    def __repr__(self):
        return f'id: {self.id}, chaturbate_username: {self.chaturbate_username}, studio_username: {self.studio_username}, apikey: {self.apikey} '


class XLoveLog(db.Model):
    __tablename__ = 'xlove_logs'

    id = db.Column('id', db.Integer, db.Sequence('xlove_logs_pk_seq'), primary_key=True)
    timestamp = db.Column('timestamp', db.DateTime, nullable=False)
    username = db.Column('username', db.String, nullable=False)
    amount = db.Column('amount', db.Float(precision=2), nullable=False)

    def __repr__(self):
        return f'id: {self.id}, timestamp: {self.timestamp}, username: {self.username}, amount: {self.amount}'


# keeps a mapping of XLove models to studio models
class XLoveModel(db.Model):
    __tablename__ = 'xlove_models'
    id = db.Column('id', db.Integer, db.Sequence('xlove_models_pk_seq'), primary_key=True)

    xlove_username = db.Column('xlove_username', db.String)
    studio_username = db.Column('studio_username', db.String, db.ForeignKey(ModelInfo.username))

    def __init__(self, _xlove_user, _local_user):
        self.xlove_username = _xlove_user
        self.studio_username = _local_user

    def __repr__(self):
        return f'id: {self.id}, xlove_username: {self.xlove_username}, studio_username: {self.studio_username}'