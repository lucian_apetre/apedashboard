from datetime import date, timedelta, datetime
import requests
import json


def format_time_ago(days):
    ago = date.today() - timedelta(days=days)
    return ago.strftime("%Y-%m-%d")


def format_today():
    now = datetime.now()
    return now.strftime("%Y-%m-%d")


TIME_INTERVALS = ['06:00:00', '14:00:00', '22:00:00']


def timestamp_to_intervals(timestamp):
    h1, m1, s1 = TIME_INTERVALS[0].split(':')
    h2, m2, s2 = TIME_INTERVALS[1].split(':')
    h3, m3, s3 = TIME_INTERVALS[2].split(':')
    tmp_date_time1 = datetime(timestamp.year, timestamp.month, timestamp.day, int(h1), int(m1), int(s1))
    tmp_date_time2 = datetime(timestamp.year, timestamp.month, timestamp.day, int(h2), int(m2), int(s2))
    tmp_date_time3 = datetime(timestamp.year, timestamp.month, timestamp.day, int(h3), int(m3), int(s3))
    start_day = datetime(timestamp.year, timestamp.month, timestamp.day, 0, 0, 0, 0)
    end_day = datetime(timestamp.year, timestamp.month, timestamp.day, 23, 59, 59, 999999)
    return start_day, tmp_date_time1, tmp_date_time2, tmp_date_time3, end_day


def create_income_table_template(start_date, end_date):
    delta_date = end_date - start_date
    if delta_date.days < 0:
        return None
    data = {}
    for day in range(delta_date.days + 1):  # +1 to include end_date
        current_date = (start_date + timedelta(days=day)).date()
        data[current_date] = {'i1': None, 'i2': None, 'i3': None, 'i4': None}
    data[end_date.date()] = {'i1': None, 'i2': None, 'i3': None, 'i4': None}
    return data


# overwrite the entire template table which contains the entire
# data table with actual value
# the result will be an 'actual restult' table starting from actual result first date
# and intervals emptied when they have no value
def append_payment_data(_template_table, actual_data):
    for day in _template_table:
        if day in actual_data:
            for interval in actual_data[day]:
                _template_table[day].update({interval: actual_data[day][interval]})
    _template_table['t_all'] = actual_data['t_all']
    return _template_table


# calculate total per day and per all days
def append_total_data(model_data):
    total_all = 0
    for day in model_data:
        total_day = 0
        for interval in model_data[day]:
            total_day = model_data[day][interval].amount
        model_data[day]['t_day'] = round(total_day, 2)
        total_all += total_day
    model_data['t_all'] = round(total_all, 2)
    return model_data


PERIOD_START = '2021-02-17'


def get_last_periods(periods_no=10):
    def is_period_start():
        known_period_start = datetime.strptime(PERIOD_START, '%Y-%m-%d')
        today_dt = datetime.now()
        delta_time = today_dt - known_period_start
        if delta_time.days % 14 == 0:
            return True
        return False

    def days_since_start_period():
        known_period_start = datetime.strptime(PERIOD_START, '%Y-%m-%d')
        today_dt = datetime.now()
        delta_time = today_dt - known_period_start
        return delta_time.days % 14

    # today starts a new period, if so, compute periods_no periods behind
    today = date.today()
    date_per_start = []
    if is_period_start():
        for per in range(periods_no):
            delta_days = per * 14
            p_last = today - timedelta(days=delta_days)
            date_per_start.append(p_last)
    else:
        days_passed = days_since_start_period()
        p_last = today - timedelta(days=days_passed)
        for per in range(periods_no):
            delta_days = per * 14
            p_before = p_last - timedelta(days=delta_days)
            date_per_start.append(p_before)
    # create start -> end intervals from the start periods array
    # reverse it to arrange it time ASC
    intervals = []
    if len(date_per_start) > 1:
        for idx, val in enumerate(date_per_start):
            index = -1 - idx
            prev_index = -1 - 1 - idx
            intervals.append((date_per_start[index], date_per_start[prev_index]))
            if date_per_start[0] == date_per_start[prev_index]:
                break
    # append current period
    if is_period_start():
        intervals.append((today,))
    else:
        intervals.append((date_per_start[0], today))
    return intervals[::-1]


def date_intervals_to_str(_date_intervals):
    date_intervals_str = []
    for item in _date_intervals:
        data_str = str(item[0])
        if len(item) == 2:
            data_str = f'{item[0]} -> {item[1]}'
        date_intervals_str.append(data_str)
    return date_intervals_str


# input all data containing USD
# return a dict containing totals for each site and grand total with all amounts
def build_totals_vertical_usd(lj_dict, bb_dict, strip_dict, chat_dict):
    lj_total = 0
    bb_total = 0
    strip_total = 0
    chat_total = 0
    for key in lj_dict:
        lj_total += lj_dict[key]
    for key in bb_dict:
        bb_total += bb_dict[key]
    for key in strip_dict:
        strip_total += strip_dict[key]
    for key in chat_dict:
        chat_total += chat_dict[key]
    return {'lj_total': lj_total,
            'bb_total': bb_total,
            'strip_total': strip_total,
            'chat_total': chat_total,
            'grand_total': lj_total + bb_total + strip_total}


# input all data containing EUR
# return a dict containing totals for each site and grand total with all amounts
def build_totals_vertical_eur(xlove_dict):
    xlove_total = 0
    for key in xlove_dict:
        xlove_total += xlove_dict[key]
    return {'xlove_total': xlove_total,
            'grand_total': xlove_total}


# input all data containing a single currency, eg USD
# return a dict containing totals for each model
def build_totals_horizontal_usd(all_keys, lj_dict, bb_dict, strip_dict, chjat_dict):
    data = {}
    for key in all_keys:
        data[key] = 0
        if key in lj_dict:
            data[key] += lj_dict[key]
        if key in bb_dict:
            data[key] += bb_dict[key]
        if key in strip_dict:
            data[key] += strip_dict[key]
        if key in chjat_dict:
            data[key] += chjat_dict[key]
    return data


# input all data containing a single currency, eg EUR
# return a dict containing totals for each model
def build_totals_horizontal_eur(all_keys, xlove_dict):
    data = {}
    for key in all_keys:
        data[key] = 0
        if key in xlove_dict:
            data[key] += xlove_dict[key]
    return data


CURRENCY_API = 'http://api.exchangeratesapi.io/v1/{}?access_key=c69003980b7afaab5c18ae73f5af317d&symbols=USD, RON'
CURRENCY_API_FIXER = 'http://data.fixer.io/api/{}?access_key=c69003980b7afaab5c18ae73f5af317d&symbols=USD, RON'


# returns the historical value for currency USD->RON
# input date
# output float representing the USD->RON parity
def get_currency_usd_ron(current_date):
    currency_date = current_date.strftime("%Y-%m-%d")
    req_url = CURRENCY_API_FIXER.format(currency_date)
    result = requests.get(req_url)
    result.raise_for_status()
    data_obj = json.loads(result.text)

    if 'success' in data_obj:
        if data_obj['success']:
            conversion_val = data_obj['rates']['RON'] / data_obj['rates']['USD']
            return round(conversion_val, 2)
    return 0


# returns the historical value for currency EUR->USD
# input date
# output float representing the EUR->USD parity
def get_currency_eur_usd(current_date):
    currency_date = current_date.strftime("%Y-%m-%d")
    req_url = CURRENCY_API_FIXER.format(currency_date)
    result = requests.get(req_url)
    result.raise_for_status()
    data_obj = json.loads(result.text)

    if 'success' in data_obj:
        if data_obj['success']:
            return round(data_obj['rates']['USD'], 2)
    return 0


CHATURBATE_API_USER_STATS = 'https://chaturbate.com/statsapi/?username={username}&token={apikey}'


# check if model and key match by verifying it directly on cha
def check_chaturbate_apikey(model, key):
    url = CHATURBATE_API_USER_STATS.format(username=model, apikey=key)
    result = requests.get(url)
    try:
        result.raise_for_status()
    except requests.exceptions.RequestException as e:
        return False
    return True
