"setup.py for uber datasource aciar"

from setuptools import setup, find_packages

DESCRIPTION = "data collector for heylux"

setup(
    name='apedashboard',
    author='Lucian Apetre',
    author_email='lucian_apetre@yahoo.com',
    url='',
    license='Proprietary ape3codelab',
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        "requests",
        "bs4",
        "flask",
        "flask-sqlalchemy",
        "flask-login",
        "psycopg2-binary"
    ]
)