# from .project import create_app
from project import create_app

if __name__ == '__main__':
    flask_app = create_app()
    flask_app.run(debug=True)